# terraform {

#   # required_version = " 1.4.6"


#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = "4.67.0"
#     }
#   }
# }

# provider "aws" {
#   region = "us-east-1"

#   default_tags {
#     tags = {
#       owner      = "paulonaka"
#       managed-by = "terraform"
#     }
#   }
# }

# Criação do bucket que armazenará os logs do cloudfront

resource "aws_s3_bucket" "bucket1" {
  bucket = var.site
}

resource "aws_s3_bucket_ownership_controls" "s3_owner" {
  bucket = aws_s3_bucket.bucket1.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "s3_pub" {
  bucket = aws_s3_bucket.bucket1.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_acl" "s3_acl" {
  depends_on = [
    aws_s3_bucket_ownership_controls.s3_owner,
    aws_s3_bucket_public_access_block.s3_pub,
  ]

  bucket = aws_s3_bucket.bucket1.id
  acl    = "public-read"
}

resource "aws_s3_bucket_website_configuration" "s3_web" {
  bucket = aws_s3_bucket.bucket1.id
  

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_s3_bucket_policy" "s3_access" {
  bucket = aws_s3_bucket.bucket1.id
  policy = data.aws_iam_policy_document.s3_access_json.json
}

data "aws_iam_policy_document" "s3_access_json" {
  statement {
    principals {
      type        = "*"
      identifiers = ["*"]
    }
    actions = ["s3:GetObject"]

    resources = ["${aws_s3_bucket.bucket1.arn}/*"]
  }
}

# Adiciona um arquivo (index.php) ao bucket
resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.bucket1.id
  content_type = "http"
  key    = "index.html"
  source = "/home/naka/site/index.html"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("/home/naka/site/index.html")
}

# Adiciona um arquivo (error.php) ao bucket
resource "aws_s3_object" "object2" {
  bucket = aws_s3_bucket.bucket1.id
  content_type = "http"
  key    = "error.html"
  source = "/home/naka/site/error.html"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("/home/naka/site/error.html")
}

# # Adiciona um arquivo (MCU2.zip) ao bucket
# resource "aws_s3_object" "object2" {
#   bucket = aws_s3_bucket.bucket1.id
#   key    = "MCU2.zip"
#   source = "/home/naka/site-teste2/MCU2.zip"

#   # The filemd5() function is available in Terraform 0.11.12 and later
#   # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
#   # etag = "${md5(file("path/to/file"))}"
#   etag = filemd5("/home/naka/site-teste2/MCU2.zip")
# }