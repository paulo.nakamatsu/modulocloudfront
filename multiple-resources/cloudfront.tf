#criar o registro no route53 com a SRD por chamado
#resource "aws_route53_record" "origin_route53_entry" {
#  zone_id = var.zone_id
#  name    = var.domain_name
#  type    = "A"
#  ttl     = 300
#  records = [var.urlip]
#}
resource "aws_cloudfront_distribution" "trt_distribution" {

  depends_on = [
    aws_wafv2_web_acl.web_acl
  ]
  
  comment         = format("%s%s","Cloudfront distribution for ",var.cliente) 
  price_class     = "PriceClass_All"
  enabled         = true
  is_ipv6_enabled = true
  web_acl_id      =  aws_wafv2_web_acl.web_acl.arn
  
  
  viewer_certificate {
    acm_certificate_arn      = var.alb_acm_certificate_arn
    iam_certificate_id = ""
    cloudfront_default_certificate = false
    minimum_protocol_version = "TLSv1.2_2021"
    ssl_support_method       = "sni-only"
  }
  
  
  aliases = [
    var.cname, var.alter_domain_name
  ]



  origin {
  
    origin_id   = var.application_name
    domain_name = format("%s%s","origin-",var.cname) 
    custom_header {
        name = "x-trt15-acesso-cloudfront"
        value = "lSsOYMzzCBeMv_FEBkBI-QjMmtVIQx"
    }
    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "match-viewer"
      origin_ssl_protocols = [
        "TLSv1.2"
      ]
    }
  }

  default_cache_behavior {
    cache_policy_id          = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad"
    #cache_policy_id          = aws_cloudfront_cache_policy.cf_cache_policy.id   #var.cache_policy_id
    origin_request_policy_id = "216adef6-5c7f-47e4-b989-5492eafa07d3"
    #origin_request_policy_id = aws_cloudfront_origin_request_policy.cf_origin_request_policy.id #var.origin_request_policy_id
    target_origin_id         = var.application_name
    viewer_protocol_policy   = "redirect-to-https"
    compress                 = true
    allowed_methods = [
      "GET",
      "HEAD",
      "OPTIONS",
      "PUT",
      "POST",
      "PATCH",
      "DELETE"
    ]

    cached_methods = [
      "GET",
      "HEAD"
    ]
  }
  
  restrictions {
    geo_restriction {
      restriction_type = "blacklist"
locations        = ["RU", "BY", "CN" , "AF", "AM" , "AZ" , "BH" , "BD" , "BT" , "IO" , "BN" , "KH" , "GE" , "IN" , "ID" , "IQ" , "IR" , "JO" , "KZ" , "KW" , "KG" , "LA" , "LB" , "MO" , "MY" , "MV" , "MN" , "MM" , "NP" , "OM" , "KP" , "PK" , "PS" , "PH" , "QA" , "SA" , "LK" , "SY" , "TJ" , "TH" , "TR" , "TM" , "AE" , "UZ" , "VN" , "YE" , "HK" ]
    }
  }

  logging_config {
    bucket = var.cf_logging_bucket
    prefix = var.application_name
  }  

}