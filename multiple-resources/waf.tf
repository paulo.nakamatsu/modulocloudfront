resource "aws_cloudwatch_log_group" "aws_waf_logs" {
  name = format("%s%s","aws-waf-logs-",var.urlname) 
  retention_in_days = "731"
  
}

resource "aws_wafv2_web_acl_logging_configuration" "waf_logging_configuration" {
  log_destination_configs = [aws_cloudwatch_log_group.aws_waf_logs.arn]
  resource_arn            = aws_wafv2_web_acl.web_acl.arn
  depends_on              = [aws_cloudwatch_log_group.aws_waf_logs]
}

resource "aws_wafv2_web_acl" "web_acl" {
  name        = format("%s%s","WAF_CF_WEB_ACL_",var.urlname)  
  description = "ACL for Backend/Frontend applications"
  scope       = "CLOUDFRONT"

  visibility_config {
    cloudwatch_metrics_enabled = true
    metric_name                = format("%s%s","WAF_CF_WEB_ACL_",var.urlname) 
    sampled_requests_enabled   = true
  }

  default_action {
    allow {}
  }

  # regra em count para bloqueio de IP estrangeiros caso necessário (mudar para block {})
  rule {
    name     = "trt15-block-non-brazil-clients"
    priority = 6
    action {
      count {}
    }

    statement {
      not_statement {
        statement {
          geo_match_statement {
            country_codes = ["BR"]
          }
        }
      }
    }    

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "trt15-block-non-brazil-clients"
      sampled_requests_enabled   = true
    }
  }
  #50 - AWSManagedRulesBotControlRuleSet
  rule {
    name     = "AWS-AWSManagedRulesBotControlRuleSet"
    priority = 11

    override_action {
      none {}
    }
    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesBotControlRuleSet"
        vendor_name = "AWS"
        managed_rule_group_configs {
            aws_managed_rules_bot_control_rule_set {
              inspection_level = "COMMON"
                        }
                    }
        rule_action_override {
          action_to_use {
            block {}
          }
          name = "Block-AWSManagedRulesBotControlRuleSet"
        }
        rule_action_override {
          action_to_use {
            count {}
          }
          name = "SignalNonBrowserUserAgent"
        }
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "AWSManagedRulesBotControlRuleSet"
      sampled_requests_enabled   = true
    }
  }

  #25 - Amazon IP reputation list
  rule {
    name     = "AWS-AWSManagedRulesAmazonIpReputationList"
    priority = 12
    override_action {
      none {}
    }
    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesAmazonIpReputationList"
        vendor_name = "AWS"

        scope_down_statement {
          not_statement {
            statement {
              ip_set_reference_statement {
                arn = aws_wafv2_ip_set.naka-reputationList-allow.arn
              }
            }
          }
        }
        rule_action_override {
          action_to_use {
            block {}
          }
          name = "AWSManagedRulesAmazonIpReputationList"
      }
        rule_action_override {
          action_to_use {
            block {}
          }
          name = "AWSManagedIPDDoSList"
        }
    }
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "AWS-AWSManagedRulesAmazonIpReputationList"
      sampled_requests_enabled   = true
    }
  }
  
  #50 - Anonymous IP List (inserir a white list)
  rule {
    name     = "AWS-AWSManagedRulesAnonymousIpList"
    priority = 13

    override_action {
      none {}
    }



    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesAnonymousIpList"
        vendor_name = "AWS"      
        scope_down_statement {
          not_statement {
            statement {
              ip_set_reference_statement {
                arn = aws_wafv2_ip_set.naka-bypassanonymousiplist.arn
              }
            }
          }
        }
        rule_action_override {
          action_to_use {
            block {}
          }
          name = "Block-AWSManagedRulesAnonymousIpList"
        }  
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "AWS-AWSManagedRulesAnonymousIpList"
      sampled_requests_enabled   = true
    }
  }
  
  #200 - Known bad inputs
  rule {
    name     = "AWS-AWSManagedRulesKnownBadInputsRuleSet"
    priority = 14

    override_action {
      none {}
    }
    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesKnownBadInputsRuleSet"
        vendor_name = "AWS"
        rule_action_override {
          action_to_use {
            block {}
          }
          name = "Block-AWSManagedRulesKnownBadInputsRuleSet"
        }
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "AWSManagedRulesKnownBadInputsRuleSet"
      sampled_requests_enabled   = true
    }
  }
  
  
  #200 - Linux operating system
  rule {
    name     = "AWS-AWSManagedRulesLinuxRuleSet"
    priority = 15

    override_action {
      count {}
    }
    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesLinuxRuleSet"
        vendor_name = "AWS"
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "AWSManagedRulesLinuxRuleSet"
      sampled_requests_enabled   = true
    }
  }
  
  #200 - SQL database
  rule {
    name     = "AWS-AWSManagedRulesSQLiRuleSet"
    priority = 16

    override_action {
      count {}
    }
    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesSQLiRuleSet"
        vendor_name = "AWS"
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "AWSManagedRulesSQLiRuleSet"
      sampled_requests_enabled   = true
    }
  }
  
  #700 - Core rule set
  rule {
    name     = "AWS-AWSManagedRulesCommonRuleSet"
    priority = 17

    override_action {
      count {}
    }

    statement {
      managed_rule_group_statement {
        name        = "AWSManagedRulesCommonRuleSet"
        vendor_name = "AWS"
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "AWSManagedRulesCommonRuleSet"
      sampled_requests_enabled   = true
    }
  }
  #Override Block AWSManagedIPDDoSList
  rule {
    name     = "Block-AWSManagedIPDDoSList"
    priority = 18

    action {
      block {}
    }
    statement {
      label_match_statement  {
        scope = "LABEL"
        key   = "awswaf:managed:aws:amazon-ip-list:AWSManagedIPDDoSList"
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "Block-AWSManagedIPDDoSList"
      sampled_requests_enabled   = true
    }
  }
  
  rule {
    name     = "Ratelimit-HostingProviderIPList"
    priority = 21

    action {
      block {}
    }
    statement {
      rate_based_statement {
        limit              = 200
        aggregate_key_type = "IP"
        scope_down_statement {
              label_match_statement {
                scope = "LABEL"
                key   = "awswaf:managed:aws:anonymous-ip-list:HostingProviderIPList"
              }
        } 
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "Block-Ratelimit-HostingProviderIPList"
      sampled_requests_enabled   = true
    }
  }

  #Implementar blanket rate based rule:
  #Esta regra serve como uma última barreira para ataques DDoS ou rajadas atípicas
  rule {
    name     = "trt15-blanket-based-rule"
    priority = 999

    action {
      block {}
    }
    statement {
      rate_based_statement {
        limit              = 900
        aggregate_key_type = "IP"                  
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "trt15-blanket-based-rule"
      sampled_requests_enabled   = true
    }
  }

}

