resource "aws_wafv2_ip_set" "<NOME_SERVICO>-bypassanonymousiplist" {
  name               = "template-bypassAnonymousIpList"
  description        = "Ipset para ignorar AnonymousIpList"  
  scope              = "CLOUDFRONT"
  ip_address_version = "IPV4"
  addresses          = []
}

resource "aws_wafv2_ip_set" "<NOME_SERVICO>-reputationList-allow" {
  name               = "template-reputationList-allow"
  description        = "Ipset criado para excluir IPs ou ranges da regra AWS-AWSManagedRulesAmazonIpReputationList."
  scope              = "CLOUDFRONT"
  ip_address_version = "IPV4"
  addresses          = []
}