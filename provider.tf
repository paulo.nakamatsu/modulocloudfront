terraform {
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "default"
    default_tags {
    tags = {
      owner      = "operacao_multicloud"
      managed-by = "terraform_site_CDN"
    }
  }
}
